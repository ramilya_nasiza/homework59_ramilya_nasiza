import React, { Component } from 'react';

import './App.css';
import MoviesDiary from "./containers/MoviesDiary/MoviesDiary";
import Joke from "./containers/Joke/Joke";

class App extends Component {

  state = {
    task2: false
  };

  showTask = () => {
    const task = this.state.task2;
    this.setState({task2: !task});
  };

  render() {
    return (
      <div className="App">
       <button onClick={this.showTask} className='pereklyuchatel'>Show other task</button>
        <MoviesDiary
            style={{display: this.state.task2 ? 'none': 'block'}}
        />
        <Joke
            style={{display: this.state.task2 ? 'block': 'none'}}
        />
      </div>
    );
  }
}

export default App;
