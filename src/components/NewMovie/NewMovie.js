import React, {Fragment} from 'react';

import './NewMovie.css';

const NewMovie = props => {
  return (
      <Fragment>
        <input value={props.value} type="text" onChange={props.change} className='input' placeholder='Fill a movie name'/>
        <button type="button" className="btnAdd" onClick={props.add}>Add</button>
      </Fragment>
  );
};

export default NewMovie;