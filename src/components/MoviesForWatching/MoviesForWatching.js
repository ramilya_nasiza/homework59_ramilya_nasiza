import React, {Component} from 'react';

import './MoviesForWatching.css';

class MoviesForWatching extends Component {

  shouldComponentUpdate(nextProps) {
    return nextProps.name !== this.props.name
  }
  render() {

    return (
        <div className='movies'>
          <span>{'#' + this.props.id}</span>
          <input value={this.props.name} onChange={event => this.props.edit(event, this.props.index)} className='editMovie'/>
          <button onClick={() => this.props.remove(this.props.id)}>X</button>
        </div>
    )
  }
}

export default MoviesForWatching;