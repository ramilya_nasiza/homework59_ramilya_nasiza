import React from 'react';

import './JokeText.css';

const JokeText = props => (
    <div className='jokeText'>{props.joke}</div>
);

export default JokeText;