import React, { Component } from 'react';
import JokeText from "../../components/JokeText/JokeText";

import './Joke.css';

class Joke extends Component {

  state = {
    joke: []
  };

  getJoke = () => {

    fetch('https://api.chucknorris.io/jokes/random')
        .then((response) => {
          if (response.ok) {
            return response.json();
          }

          throw new Error('Request is not correct');
        })
        .then((joke) => {
          console.log(joke);
          this.setState({joke})
        })
  };

  componentDidMount(){
    this.getJoke();
  }
  
  render() {
    return (
      <div style={this.props.style} className='Joke'>
        <img src={this.state.joke.icon_url} alt="" className='icon'/>
        <JokeText
        joke={this.state.joke.value}
        />
      </div>
    );
  }
}

export default Joke;
