import React, {Component} from 'react';

import './MoviesDiary.css';
import NewMovie from "../../components/NewMovie/NewMovie";
import MoviesForWatching from "../../components/MoviesForWatching/MoviesForWatching";

class MoviesDiary extends Component {

  state ={
    movies: [],
    name: ''
  };

  changeHandler = (event) => {
    this.setState({
      name: event.target.value
    })
  };

  saveToLocal = () =>{
    const local = this.state.movies;
    localStorage.setItem('movies', JSON.stringify(local));
  };

  addNewMovie = () => {
    const movies = [...this.state.movies];
    if(this.state.name !== '') {
      const movie = {name: this.state.name, id: movies.length + 1};
      movies.push(movie);

      this.setState({movies: movies, name: ''}, this.saveToLocal);
    } else {
      alert('Fill the name!')
    }
  };

  removeMovie = (id) =>{
    const movies = [...this.state.movies];
    const movieId = movies.findIndex(oneMovie => oneMovie.id === id);

    movies.splice(movieId, 1);

    this.setState({movies}, this.saveToLocal);
  };

  tryChangeName = (event, index) =>{
    const movies = [...this.state.movies];
    const movie = {...movies[index]};
    movie.name = event.target.value;
    movies[index] = movie;
    this.setState({movies}, this.saveToLocal);
  };

  componentDidMount() {
    const movies = JSON.parse( localStorage.getItem( "movies" ) );
    this.setState({movies});
  }

  render() {
    return (
        <div style={this.props.style} className='diary'>
          <NewMovie
              value={this.state.name}
              change={this.changeHandler}
            add={event => this.addNewMovie(event)}
          />
          <div className='allMovies'>
            {this.state.movies.map((movie, index) => {
              return (
                  <MoviesForWatching
                    key={index}
                    name={movie.name}
                    edit={this.tryChangeName}
                    remove={this.removeMovie}
                    id={movie.id}
                    index={index}
                  />
              )
            })}
          </div>
        </div>
    );
  }
}

export default MoviesDiary;
